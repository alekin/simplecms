# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.contenttypes.models import ContentType

from pages.models import Page
from pages.utils import get_available_page_templates


class PageForm(forms.ModelForm):
    template_label = Page._meta.get_field('template').verbose_name
    template = forms.ChoiceField(label=template_label)

    class Meta:
        model = Page
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        # Список доступных шаблонов, по-умолчанию выбран default
        super(PageForm, self).__init__(*args, **kwargs)
        template_choices = [('pages/%s.html' % template, template)
                            for template in get_available_page_templates()]
        self.fields['template'].initial = 'pages/default.html'
        self.fields['template'].choices = template_choices

        # Убираем возможность ручного ввода URL страницы
        self.fields['url'].widget.attrs['readonly'] = True


class RelatedPageForm(forms.ModelForm):
    page = forms.ModelChoiceField(Page.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super(RelatedPageForm, self).__init__(*args, **kwargs)
        instance = kwargs.pop('instance', None)
        if instance:
            try:
                page = (ContentType.objects.get_for_model(Page)
                        .get_object_for_this_type(object_id=instance.id))
            except ObjectDoesNotExist:
                page = None
            self.fields['page'].initial = page
