# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from django.views.generic import DetailView

from pages.models import Page


class PageView(DetailView):
    """
    Просмотр статической страницы. Ссылка на шаблон из модели.
    """
    model = Page
    _plugin = None

    def get_template_names(self):
        return getattr(self._plugin, 'template', self.object.template)

    def get_object(self):
        url = self.kwargs.get('url', None)
        if url:
            obj = get_object_or_404(self.model, url=url)
            if obj.content_object:
                self._plugin = obj.content_object
        else:
            obj = get_object_or_404(self.model, is_home=True)
        return obj

    def get_context_data(self, **kwargs):
        context = {
            'plugin': self._plugin,
            'extend_template_name': self.object.template,
        }
        context.update(kwargs)
        return super(PageView, self).get_context_data(**context)


class BasePluginView(DetailView):
    _page = None

    def get_object(self):
        self._page = get_object_or_404(Page, url=self.kwargs.get('page'))

    def get_context_data(self, **kwargs):
        context = {
            'page': self._page,
            'extend_template_name': self._page.template,
        }
        context.update(kwargs)
        return super(BasePluginView, self).get_context_data(**context)
