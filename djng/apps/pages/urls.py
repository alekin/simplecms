# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns

from pages import views


urlpatterns = patterns(
    '',
    url(r'^$', views.PageView.as_view(), name='page_home'),
    url(r'^(?P<url>.*)/$', views.PageView.as_view(), name='page_view'),
)
