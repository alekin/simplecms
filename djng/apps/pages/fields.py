# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
from django.db.models.fields import SlugField
from django.forms.fields import CharField
from slugify import slugify


def validate_slug(value):
    s = slugify(value)
    if value != s:
        raise ValidationError(u'Неправильный алиас. Подсказка: %s',
                              params=(s,), code='error')


class UnicodeSlugFormField(CharField):
    default_validators = [validate_slug]

    def clean(self, value):
        value = self.to_python(value).strip()
        return super(UnicodeSlugFormField, self).clean(value)


class UnicodeSlugField(SlugField):
    default_validators = [validate_slug]

    def formfield(self, **kwargs):
        defaults = {'form_class': UnicodeSlugFormField}
        defaults.update(kwargs)
        return super(UnicodeSlugField, self).formfield(**defaults)
