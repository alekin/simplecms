# -*- coding: utf-8 -*-
from django_mptt_admin.admin import DjangoMpttAdmin

from django.contrib import admin
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist

from pages.models import Page
from pages.forms import PageForm


class BaseAdmin(admin.ModelAdmin):
    """
    Базовый класс администирования.
    """
    def save_model(self, request, obj, form, change):
        obj.author = request.user
        obj.save()


@admin.register(Page)
class PageAdmin(DjangoMpttAdmin, BaseAdmin):
    """
    Администрирование модели "Страница".
    """
    form = PageForm
    tree_load_on_demand = False
    fields = (('title', 'slug'), 'menu_name', 'css', 'body',
              ('is_published', 'is_home'), 'parent', 'template', 'author',
              'created', 'url')

    def do_move(self, instance, position, target_instance):
        """
        Обновление URL после перемещения страницы в дереве.
        """
        super(PageAdmin, self).do_move(instance, position, target_instance)
        instance.update_urls()


class MixinRelatedPageAdmin(object):
    """
    Примесь для связи моделей.
    """
    def save_model(self, request, obj, form, change):
        if form.is_valid():
            obj.save()

            ct_page = ContentType.objects.get_for_model(Page)

            try:
                old_page = ct_page.get_object_for_this_type(object_id=obj.id)
                old_page.content_type = None
                old_page.object_id = None
                old_page.save()
            except ObjectDoesNotExist:
                pass

            try:
                page_pk = request.POST.get('page', None)
                if page_pk:
                    page = Page.objects.get(pk=page_pk)
                    page.content_type = (
                        ContentType.objects.get_for_model(self.model))
                    page.object_id = obj.id
                    page.save()
            except ObjectDoesNotExist:
                pass

    def delete_model(self, request, obj):
        """
        Прежде чем удалить объект, убираем связь, чтоб не удалить связаные
        объекты.
        """
        try:
            ct_page = ContentType.objects.get_for_model(Page)
            page = ct_page.get_object_for_this_type(object_id=obj.id)
            page.content_type = None
            page.object_id = None
            page.save()
        except ObjectDoesNotExist:
            pass
        obj.delete()
