# -*- coding: utf-8 -*-
from mptt.models import MPTTModel, TreeForeignKey

from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
# from django.utils.functional import cached_property

from pages.fields import UnicodeSlugField


class BaseModel(models.Model):
    """
    Базовая модель.
    """
    created = models.DateTimeField(
        u'Дата создания',
        default=timezone.now)
    updated = models.DateTimeField(
        u'Дата изменения',
        auto_now=True)
    is_published = models.BooleanField(
        u'Опубликовано', default=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=u'Автор',
        null=True, blank=True)

    class Meta:
        abstract = True


class Page(MPTTModel, BaseModel):
    """
    Модель для статических страниц
    """
    title = models.CharField(
        u'Название', max_length=200)
    body = models.TextField(
        u'Содержимое', blank=True)
    slug = UnicodeSlugField(
        u'Алиас', max_length=200)
    menu_name = models.CharField(
        u'Пункт меню', max_length=100, blank=True)
    template = models.CharField(
        u'Шаблон', max_length=200)
    is_home = models.BooleanField(
        u'Главная', default=False, blank=True)
    parent = TreeForeignKey(
        'self',
        verbose_name=u'Родительская страница',
        null=True, blank=True, related_name='children')
    url = models.CharField(
        u'Ссылка', max_length=200, blank=True)
    css = models.SlugField(
        u'CSS class', max_length=20, blank=True)
    content_type = models.ForeignKey(
        ContentType, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        verbose_name = u'Страница'
        verbose_name_plural = u'Страницы'
        unique_together = (('parent', 'slug'), ('content_type', 'object_id'))

    def __unicode__(self):
        return self.title if not self.is_home else u'%s [\u2605]' % self.title

    def save(self, *args, **kwargs):
        super(Page, self).save(*args, **kwargs)
        self.uniq_home_page()
        self.update_urls()

    def get_absolute_url(self):
        return (reverse('page_view', kwargs={'url': self.url})
                if not self.is_home else '')

    def get_url(self):
        """
        Формируем URL, учитывая родителей.
        """
        slug_list = []
        if not self.is_home:
            instance = self
            while instance is not None:
                slug_list.append(getattr(instance, 'slug'))
                instance = getattr(instance, 'parent')
        return '/'.join(reversed(slug_list)) if slug_list else ''

    @classmethod
    def update_urls(cls, obj=None):
        if obj:
            pages = obj if type(obj) is list else [obj]
        else:
            pages = cls.objects.all()
        for page in pages:
            cls.objects.filter(pk=page.pk).update(url=page.get_url())

    def uniq_home_page(self):
        if self.is_home:
            (self.__class__.objects.filter(is_home=True).exclude(pk=self.pk)
                 .update(is_home=False))
