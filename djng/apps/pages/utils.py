# -*- coding: utf-8 -*-
import os
from django.template.loaders.app_directories import app_template_dirs


def get_available_page_templates():
    """
    Получаем названия доступных шаблонов для Pages во всех приложениях проекта.
    """
    template_files = []
    for template_dir in app_template_dirs:
        pages_template_dir = '%s/pages' % template_dir
        if os.path.isdir(pages_template_dir):
            for file in os.listdir(pages_template_dir):
                if file.endswith(('.html', '.haml')):
                    template_files.append(file[:-5])
    return list(set(template_files))
