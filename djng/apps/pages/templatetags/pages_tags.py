# -*- coding: utf-8 -*-
from django import template

from pages.models import Page

register = template.Library()


class TreeNavigationNode(template.Node):
    model = Page

    def __init__(self, context_var):
        self.context_var = context_var

    def render(self, context):
        context['available_pages'] = (self.model.objects
                                                .filter(is_published=True)
                                                .exclude(menu_name__exact=''))
        context['parents_page'] = context['page'].get_ancestors(
            include_self=True)
        tpl = template.loader.get_template('navigation/tree_navigation.html')
        context[self.context_var] = tpl.render(context)
        return ''


@register.tag
def tree_navigation(parser, token):
    bits = token.contents.split()
    if len(bits) != 3:
        raise template.TemplateSyntaxError(
            '%s tag required two arguments' % bits[0])
    if bits[1] != 'as':
        raise template.TemplateSyntaxError(
            'first argument to %s tag must be "as"' % bits[0])
    return TreeNavigationNode(bits[2])
