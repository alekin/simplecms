# -*- coding: utf-8 -*-
from pages.admin import MixinRelatedPageAdmin
from pages.forms import RelatedPageForm

from django.contrib import admin

from catalog.models import Catalog, Category


@admin.register(Catalog)
class CatalogAdmin(MixinRelatedPageAdmin, admin.ModelAdmin):
    form = RelatedPageForm


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    fields = ('catalog', 'parent', ('name', 'slug'))
    list_display = ('name', 'catalog')
    list_filter = ('catalog', )

    _obj = None

    def get_readonly_fields(self, request, obj=None):
        fields = [field for field in self.readonly_fields]
        if obj:
            fields.append('catalog')
        else:
            fields.append('parent')
        return fields

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self._obj = obj
        return super(CategoryAdmin, self).get_form(request, obj, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name is 'parent':
            obj = self._obj
            if obj:
                kwargs['queryset'] = (
                    self.model.objects.filter(catalog=obj.catalog))
        return super(CategoryAdmin,
                     self).formfield_for_foreignkey(db_field, request,
                                                    **kwargs)
