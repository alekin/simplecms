# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from catalog import views


urlpatterns = patterns(
    '',
    url(r'^(?P<page>.*)/module-catalog/$',
        views.RedirectToPageView.as_view()),
    url(r'^(?P<page>.*)/module-catalog/(?P<slug>[-\w]+)/$',
        views.CategoryPluginView.as_view(), name='category_view'),
)
