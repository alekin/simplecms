# -*- coding: utf-8 -*-
from pages import views as pages_view

from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views.generic import RedirectView

from catalog.models import Category


class RedirectToPageView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.request.path[-1]:
            path = self.request.path[:-1]
        else:
            path = self.request.path
        return path.rsplit('/', 1)[0]


class CategoryPluginView(pages_view.BasePluginView):
    model = Category
    template_name = 'catalog/category.html'

    def get_object(self):
        super(CategoryPluginView, self).get_object()
        obj = get_object_or_404(self.model, slug=self.kwargs.get('slug'))
        if not obj.page == self._page:
            raise Http404()
        return obj
