# -*- coding: utf-8 -*-
from mptt.models import MPTTModel, TreeForeignKey

from pages import models as pages_models
from pages.fields import UnicodeSlugField

from django.contrib.contenttypes.generic import GenericRelation
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.functional import cached_property


class Catalog(models.Model):
    """
    Модель для каталога.
    """
    template = 'catalog/catalog.html'

    name = models.CharField(
        u'Название', max_length=200)
    pages = GenericRelation(pages_models.Page, related_query_name='catalog')

    class Meta:
        verbose_name = u'Каталог'
        verbose_name_plural = u'Каталоги'

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return (reverse('page_view', kwargs={'url': self.page.url})
                if not self.page.is_home else '')

    @cached_property
    def page(self):
        return self.pages.last()


class Category(MPTTModel, models.Model):
    """
    Модель "Категория".
    """
    name = models.CharField(
        u'Название', max_length=200)
    slug = UnicodeSlugField(
        u'Алиас', max_length=200, unique=True)
    parent = TreeForeignKey(
        'self',
        verbose_name=u'Родительская категория',
        null=True, blank=True, related_name='children')
    catalog = models.ForeignKey(
        'Catalog',
        verbose_name=u'Каталог',
        related_name='category')

    class Meta:
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category_view', kwargs={'page': self.catalog.page.url,
                                                'slug': self.slug})

    @cached_property
    def page(self):
        return self.catalog.pages.last()
