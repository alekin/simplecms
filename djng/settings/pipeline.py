# -*- coding: utf-8 -*-
import os

PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
path = lambda *a: os.path.join(PROJECT_ROOT, *a)


def sass_load_paths(*apps):
    """
    Пути к sass-шаблонам, чтобы можно быть делать кросс-импорты.
    """
    return ' '.join('--load-path %s/static' % path(app)
                    for app in apps)

PIPELINE_SASS_ARGUMENTS = '-q '
PIPELINE_SASS_ARGUMENTS += sass_load_paths('', )

PIPELINE_CSS = {
    # 'admin': {
    #     'source_filenames': (
    #         'scss/admin.scss',
    #     ),
    #     'output_filename': 'css/admin.css',
    # },
}

PIPELINE_JS = {
    # 'admin': {
    #     'source_filenames': (
    #         'bower_components/jquery/dist/jquery.min.js',
    #     ),
    #     'output_filename': 'js/admin.settings.js',
    # },
}
